package com.example.dhht.arcface.app;

import android.app.Application;

import com.example.arclibrary.manager.ArcFaceManager;
import com.yorhp.picturepick.PicturePickUtil;

public class MyApplication extends Application {
    private static Application application;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        ArcFaceManager.setFreeSdkAppId(Constants.FREESDKAPPID);
        ArcFaceManager.setFdSdkKey(Constants.FDSDKKEY);
        ArcFaceManager.setFtSdkKey(Constants.FTSDKKEY);
        ArcFaceManager.setFrSdkKey(Constants.FRSDKKEY);
        PicturePickUtil.init("com.yorhp.arcface.fileProvider");
    }

    public static Application getApplication() {
        return application;
    }

}



