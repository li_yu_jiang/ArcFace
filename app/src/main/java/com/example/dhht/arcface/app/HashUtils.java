package com.example.dhht.arcface.app;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by liyujiang on 2019/12/01 01:34
 *
 * @author 大定府羡民
 */
public class HashUtils {

    /**
     * 计算哈希值，算法可以是MD2、MD5、SHA-1、SHA-256等
     */
    public static String hash(byte[] data, String algorithm) {
        try {
            byte[] bytes = MessageDigest.getInstance(algorithm).digest(data);
//            char chars[] = new char[bytes.length * 2];
//            final char HEX_CODE[] = {'0', '1', '2', '3', '4', '5', '6', '7',
//                    '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
//            int i = 0;
//            for (byte b : bytes) {
//                chars[i++] = HEX_CODE[b >>> 4 & 0xf];
//                chars[i++] = HEX_CODE[b & 0xf];
//            }
//            String str = new String(chars);
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(String.format("%02x", aByte));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    /**
     * 计算SHA1
     */
    public static String sha1(String str) {
        return hash(str.getBytes(), "SHA-1");
    }

    /**
     * 计算MD5
     */
    public static String md5(String str) {
        return hash(str.getBytes(), "MD5");
    }

}
