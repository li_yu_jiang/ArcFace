package com.example.dhht.arcface;

import android.Manifest;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arcsoft.facerecognition.AFR_FSDKFace;
import com.arcsoft.facetracking.AFT_FSDKFace;
import com.example.arclibrary.facerecognition.FaceRecognitionService;
import com.example.arclibrary.facerecognition.FaceSerchListener;
import com.example.arclibrary.util.ImageUtils;
import com.example.dhht.arcface.app.HashUtils;
import com.example.dhht.arcface.camera.ArcFaceCamera;
import com.example.dhht.arcface.camera.CameraPreviewListener;

import java.util.ArrayList;
import java.util.List;

import permison.PermissonUtil;
import permison.listener.PermissionListener;

public class FaceActivity extends AppCompatActivity implements CameraPreviewListener {
    public static List<Face> faces = new ArrayList<>();
    SurfaceView surfce_preview, surfce_rect;
    TextView tv_status, tv_name;
    ImageView iv_face;

    //相机的方向
    private int cameraOri = 90;
    public static int flag = 0;

    FaceRecognitionService faceRecognitionService;
    //比对阈值，需要大于7.0，建议为0.82
    private static final double THRESHOLD = 0.82d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_face);
        tv_status = (TextView) findViewById(R.id.tv_status);
        tv_name = (TextView) findViewById(R.id.tv_name);
        surfce_preview = (SurfaceView) findViewById(R.id.surfce_preview);
        surfce_rect = (SurfaceView) findViewById(R.id.surfce_rect);
        iv_face = (ImageView) findViewById(R.id.iv_face);

        faceRecognitionService = new FaceRecognitionService();

        final ArcFaceCamera arcFaceCamera = new ArcFaceCamera();
        arcFaceCamera.setCameraPreviewListener(this);
        arcFaceCamera.init(Camera.CameraInfo.CAMERA_FACING_FRONT);

        PermissonUtil.checkPermission(this, new PermissionListener() {
            @Override
            public void havePermission() {
                arcFaceCamera.openCamera(FaceActivity.this, surfce_preview, surfce_rect);
            }

            @Override
            public void requestPermissionFail() {
                finish();
            }
        }, Manifest.permission.CAMERA);
    }

    //开始检测
    public synchronized void detect(final byte[] data, final List<AFT_FSDKFace> fsdkFaces) {
        if (fsdkFaces.size() > 0) {//如果有人脸进行注册、识别
            final AFT_FSDKFace aft_fsdkFace = fsdkFaces.get(0).clone();
            if (flag == 1) {
                //人脸注册-----------------------------------------------------------------------------------------------------------
                AFR_FSDKFace afr_fsdkFace = faceRecognitionService.faceData(data, aft_fsdkFace.getRect(), aft_fsdkFace.getDegree());
                Face face = new Face(faces.size() + 1, afr_fsdkFace.getFeatureData());
                faces.add(face);
                toast("注册成功，Id=" + face.getId() + ",Hash=" + face.getHash());
                finish();
                flag = -1;
            } else if (flag == 2) {
                //人脸对比----------------------------------------------------------------------------------------------
                AFR_FSDKFace afr_fsdkFace = faceRecognitionService.faceData(data, aft_fsdkFace.getRect(), aft_fsdkFace.getDegree());
                List<byte[]> faceList = new ArrayList<>();
                for (Face face : faces) {
                    faceList.add(face.getData());
                }
                byte[] featureData = afr_fsdkFace.getFeatureData();
                tv_status.setText(String.format("人脸特征为：%s", HashUtils.hash(featureData, "MD5")));
                faceRecognitionService.faceSerch(featureData, faceList, new FaceSerchListener() {
                    @Override
                    public void serchFinish(float sorce, int position) {
                        Log.e("FaceActivity", "sorce：" + sorce + "，position：" + position);
                        if (faces.size() == 0) {
                            toast("还没有注册过人脸");
                            return;
                        }
                        tv_name.setText(String.format("Id: %s, 相似度：%s", faces.get(position).getId(), sorce));
                        if (sorce > THRESHOLD) {
                            tv_name.append("，是同一人");
                            iv_face.setImageBitmap(ImageUtils.cropFace(data, aft_fsdkFace.getRect(), mWidth, mHeight, cameraOri));
                        } else {
                            tv_name.append("，请调整头像位置");
                        }
                    }
                });
                flag = -1;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(500);
                            flag = 2;
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        }
    }

    public void toast(final String test) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(FaceActivity.this, test, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        faceRecognitionService.destroyEngine();
    }

    @Override
    public void onPreviewData(byte[] data, List<AFT_FSDKFace> fsdkFaces) {
        detect(data, fsdkFaces);
    }

    int mWidth, mHeight;

    @Override
    public void onPreviewSize(int width, int height) {
        mHeight = height;
        mWidth = width;
        faceRecognitionService.setSize(width, height);
    }
}
