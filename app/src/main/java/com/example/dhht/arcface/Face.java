package com.example.dhht.arcface;

import com.example.dhht.arcface.app.HashUtils;

import java.io.Serializable;

public class Face implements Serializable {
    private int id;
    private String hash;
    private byte[] data;

    public Face(int id, byte[] data) {
        this.id = id;
        this.hash = HashUtils.hash(data, "MD5");
        this.data = data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

}
