package com.example.arclibrary.manager;

import com.example.arclibrary.facefind.FaceFindService;
import com.example.arclibrary.facerecognition.FaceRecognitionService;
import com.example.arclibrary.facetrack.FaceTrackService;

/**
 * 初始化sdkKey和appId
 */
public class ArcFaceManager {

    private ArcFaceManager() {
        super();
    }

    public static void setFreeSdkAppId(String freeSdkAppId) {
        FaceFindService.setAPPID(freeSdkAppId);
        FaceTrackService.setAPPID(freeSdkAppId);
        FaceRecognitionService.setAPPID(freeSdkAppId);
    }

    public static void setFdSdkKey(String fdSdkKey) {
        FaceFindService.setFdSdkkey(fdSdkKey);
    }

    public static void setFtSdkKey(String ftSdkKey) {
        FaceTrackService.setFtSdkkey(ftSdkKey);
    }

    public static void setFrSdkKey(String frSdkKey) {
        FaceRecognitionService.setFrSdkkey(frSdkKey);
    }

}
