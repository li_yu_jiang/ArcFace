﻿# Android集成虹软人脸识别SDK

虹软人脸识别SDK获取：https://ai.arcsoft.com.cn/product/arcface.html

#### 友情提示
> 只适用于v1.2版本，现在官网人脸识别有v2.0、v2.2、v3.0，因为申请的时候下载的so库和给的AppKey是关联的，所以一定要把so库和Constants类下面的各种AppKey和AppId换成自己的才能正常运行。

### 虹软人脸识别库的介绍
#### so库和appkey是绑定的
以人脸识别为例，它包括**人脸检测（FD）、人脸追踪（FT）、人脸识别（FR）、年龄识别（Age）、性别识别（Gender）**这5种引擎，每个引擎都有一个**so库和jar包**，申请的5种**AppKey**和**AppId**是和自己下载的so库是绑定的，不能混淆使用。

#### 人脸检测(FaceDetection)
用于获取静态图片的人脸的位置和角度，传入格式为**NV21**的图片数据(byte[])，返回一个**AFD_FSDKFace**对象的集合，**AFD_FSDKFace**只储存了一个位置和角度；如果用于视频流里面好像也不报错。

```java
public class AFD_FSDKFace {
    Rect mRect;
    int mDegree;
    ...
```

#### 人脸追踪(FaceTracking)
和人脸检测一样，也是用来获取人脸的位置和角度，不过只适用于获取视频流的人脸，也就是在相机的**onPreviewFrame**方法里面使用，返回的是**AFT_FSDKFace**对象的集合，也只储存了一个位置和角度；如果用于静态图片好像是会报错的。

```java
public class AFT_FSDKFace {
    Rect mRect;
    int mDegree;
    ...
```

#### 人脸识别(FaceRecognition)
用于获取人脸特征和对比人脸特征的
获取人脸特征，需要传入格式为**NV21**的图片数据(byte[])和人脸的位置、人脸的角度，所以需要先用前面的引擎获取到人脸的信息,返回一个**AFR_FSDKFace**对象，这个对象也只保存了人脸特征(byte[])
对比人脸，需要传入两个**AFR_FSDKFace**对象，返回一个**AFR_FSDKMatching**对象，只保存了相似度。

```java
public class AFR_FSDKFace {
    public static final int FEATURE_SIZE = 22020;
    byte[] mFeatureData;
    ...
    
public class AFR_FSDKMatching {
    float mScore = 0.0F;
    ...
```

其他的年龄、性别识别的引擎应该都差不多。

### 封装后的部分功能的展示

#### 初始化AppKey和AppId
```java
new AcrFaceManagerBuilder().setContext(this)
                .setFreeSdkAppId(Constants.FREESDKAPPID)
                .setFdSdkKey(Constants.FDSDKKEY)
                .setFtSdkKey(Constants.FTSDKKEY)
                .setFrSdkKey(Constants.FRSDKKEY)
                .create();
    }
```

#### 相机预览追踪人脸位置
```java
//初始化人脸追踪引擎
FaceTrackService faceTrackService = new FaceTrackService();
//设置传入的图片的大小
faceTrackService.setSize(previewSize.width, previewSize.height);
 camera.setPreviewCallback(new Camera.PreviewCallback() {
                @Override
                public void onPreviewFrame(byte[] data, Camera camera) {
                    //获取人脸的位置信息
                    List<AFT_FSDKFace> fsdkFaces = faceTrackService.getFtfaces(data);
                    //画出人脸的位置
                    drawFaceRect(fsdkFaces);
                    //输出数据进行其他处理
                    cameraPreviewListener.onPreviewData(data.clone(), fsdkFaces);
                    ...
                }
            });
```
相机自己实现，获取人脸位置的代码非常简单，就一句代码，画出人脸的位置实现是用了两个surfaceView，一个用于相机画面展示，另一个画出人脸的位置。

#### 画出人脸的位置
值得注意的是获取的人脸的位置Rect是传入的图片的相对位置，图片大小是相机预览设置的大小，画的时候是画在了surfaceView上面，surfaceView一般和预览大小是不一样的，而且还要考虑画面是否旋转、相机的位置等，所以需要先进行转换。
```java
Rect rect1=DrawUtils.adjustRect(rect, previewSizeX, previewSizeY,canvas.getWidth(), canvas.getHeight(), cameraOri, cameraId);
```

#### 获取人脸特征进行注册
```java
//初始化人脸识别引擎
FaceRecognitionService faceRecognitionService = new FaceRecognitionService();
faceRecognitionService.setSize(width, height);
//获取人脸特征
AFR_FSDKFace afr_fsdkFace =faceRecognitionService.faceData(data, aft_fsdkFace.getRect(), aft_fsdkFace.getDegree());
tv_status.setText("人脸特征为：" + afr_fsdkFace.getFeatureData());
```
**aft_fsdkFace**为上一步获取的人脸的位置信息。

#### 相机获取的人脸和已保存的人脸进行对比
```java
//获取保存的人脸特征
byte[] faceData=faces.get(0).getData();
//对比人脸特征
float socre=faceRecognitionService.faceRecognition(afr_fsdkFace.getFeatureData(),faceData);
tv_status.setText("相似度为：" + sorce);
```
**afr_fsdkFace**为上一步获取的人脸的特征，**faceData**为已保存的人脸特征，也有提供一个人脸和多个对比获取相似度最高的一个的方法。

#### 图片获取人脸特征
```java
//先把bitmap转NV21格式
byte[] photoData = ImageUtils.getNV21(bitmap1.getWidth(), bitmap1.getHeight(), bitmap1);
//获取人脸位置信息
List<AFD_FSDKFace> afd_fsdkFaces = faceFindService.findFace(photoData);
for (AFD_FSDKFace afdFsdkFace : afd_fsdkFaces) {
//获取每一个人脸的特征
AFR_FSDKFace afr_fsdkFace = faceRecognitionService.faceData(photoData, afdFsdkFace.getRect(), afdFsdkFace.getDegree());
}
```
#### 引擎释放
```java
livenessService.destoryEngine();
faceTrackService.destoryEngine();
faceRecognitionService.destroyEngine();
```

效果都还不错，主要是全部免费，下载源码，替换so库和AppKey、sdkKey，才能运行，可以查看所有功能。   

Demo下载地址：[arcface v1.2 demo](/app-debug.apk)   

项目地址（本库移除了原库里的活体检测及认证对比部分）：https://github.com/tyhjh/Arcface   
